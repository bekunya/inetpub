﻿using Brokeree.Core.PInvoke;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brokeree.Core.Utils
{
   public interface IDataSerializer
    {
        byte[] Serialize(Header header, TradeRecord[] records);
        Tuple<Header, TradeRecord[]> Deserialize(byte[] array);
        Header DeserializeHeader(byte[] array);
        TradeRecord[] DeserializeTradeRecords(byte[] array);
    }
}
