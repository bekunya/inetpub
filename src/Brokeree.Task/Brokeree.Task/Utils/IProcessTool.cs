﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brokeree.Core.Utils
{
   public interface IProcessTool
    {
        void Start(string fileName, string args, string workDirectory);

    }
}
