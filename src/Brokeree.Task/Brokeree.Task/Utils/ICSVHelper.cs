﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brokeree.Core.Utils
{
    public interface ICSVHelper
    {
        void CreateCSV<T>(List<T> list, string csvCompletePath);
    }
}
