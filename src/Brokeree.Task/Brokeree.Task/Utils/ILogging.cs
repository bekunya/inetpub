﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brokeree.Core.Utils
{
    public interface ILogging
    {
        void Configure(string path = "./log4net.config");
        void Info(string info);
        void Error(string error);
    }
}
