﻿using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using log4net;
using log4net.Config;

namespace Brokeree.Core.Utils.Impl
{
    public class Logging : ILogging
    {

        private readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void Configure(string path = "./log4net.config")
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException();
            }

            LogManager.ResetConfiguration();

            XmlConfigurator.Configure(new FileInfo(path));
        }

        public void Info(string info)
        {
            Logger.Info(info);
        }

        public void Error(string error)
        {
            Logger.Info(error);
        }

    }
}
