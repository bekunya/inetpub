﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Brokeree.Core.Utils.Impl
{
    public class CSVHelper : ICSVHelper
    {
        public void CreateCSV<T>(List<T> list, string csvCompletePath)
        {
            if (list == null || list.Count == 0) return;

            Type t = list[0].GetType();
            string newLine = Environment.NewLine;

            if (!Directory.Exists(Path.GetDirectoryName(csvCompletePath))) Directory.CreateDirectory(Path.GetDirectoryName(csvCompletePath));

            if (!File.Exists(csvCompletePath)) File.Create(csvCompletePath);

            using (var sw = new StreamWriter(csvCompletePath))
            {
                object o = Activator.CreateInstance(t);

                PropertyInfo[] props = o.GetType().GetProperties();

                sw.Write(string.Join(",", props.Select(d => d.Name).ToArray()) + newLine);

                foreach (T item in list)
                {
                    var row = string.Join(",", props.Select(d => item.GetType()
                                                                    .GetProperty(d.Name)
                                                                    .GetValue(item, null)
                                                                    .ToString())
                                                            .ToArray());
                    sw.Write(row + newLine);

                }
            }
        }
    }
}
