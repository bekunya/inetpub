﻿using System;
using System.Globalization;
using Brokeree.Core.Exceptions;
using Brokeree.Core.Models;
using Brokeree.Core.PInvoke;

namespace Brokeree.Core.Extensions
{
    public static class ConvertExtension
    {
        public static Models.Header AsModel(this Tuple<PInvoke.Header, PInvoke.TradeRecord[]> item)
        {
            //header
            var model = new Models.Header
            {
                Type = item.Item1.type,
                Version = item.Item1.version
            };

            //header
            foreach (var record in item.Item2)
            {
                model.TradeRecords.Add(new Models.TradeRecord
                {
                    Volumne = record.volume,
                    Account = record.account,
                    Id = record.id,
                    Comment = record.comment
                });
            }

            return model;
        }
    }
}