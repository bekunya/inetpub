﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Brokeree.Core.Models
{
    public class Header
    {
        [Key]
        public int Version { get; set; }

        [Required]
        [StringLength(16)]
        public string Type { get; set; }

        public virtual ICollection<TradeRecord> TradeRecords { get; set; }

        public Header()
        {
            TradeRecords = new HashSet<TradeRecord>();
        }

    }
}
